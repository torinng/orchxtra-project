# orchxtra-project

## Introduction

Orchxtra Project REST Service.

## Requirement
- stack
- docker
- AWS CLI
- AWS API Gateway
- AWS CodeBuild
- AWS Elastic Container Service

## Deployment 

- Build docker image with AWS CodeBuild
- Deploy to Elastic Container Service and API Gateway using Cloudformation Stack:

Create Stack for first time deployment
```
sh ./cloudformation/create-stack.sh
```

Update Stack
```
sh ./cloudformation/update-stack.sh
```
