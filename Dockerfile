FROM fpco/stack-build:lts-11.11 as build
RUN mkdir /opt/build
COPY . /opt/build
RUN cd /opt/build && stack build -j1 --system-ghc 
FROM ubuntu:16.04
RUN mkdir -p /opt/myapp
ARG BINARY_PATH
WORKDIR /opt/myapp
RUN apt-get update && apt-get install -y \
  ca-certificates \
  libgmp-dev \
  libpq-dev
  
# NOTICE THIS LINE
COPY --from=build /opt/build/.stack-work/install/x86_64-linux/lts-11.11/8.2.2/bin .
CMD ["/opt/myapp/orchxtra-project-exe"]
