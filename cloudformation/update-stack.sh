#!/bin/bash

config=$(<config.json)
template=$(<../ecs.yaml)

aws cloudformation update-stack \
    --stack-name=orchxtra-projects \
    --template-body="$template" \
    --parameters="$config"
