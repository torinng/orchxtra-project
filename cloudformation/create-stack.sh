#!/bin/bash

config=$(<./config.json)
template=$(<../ecs.yaml)

aws cloudformation create-stack \
  --stack-name=orchxtra-projects \
  --template-body="$template" \ 
  --parameters="$config" \ 
  --on-failure=DELETE
