{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Lib
    ( main
    ) where

import Web.Scotty

import Control.Monad.Reader

import qualified Common.PG as PG 
import qualified Platform.HTTP as HTTP
import qualified Module.Project.HTTP as ProjectHTTP 
import qualified Module.Project.PG as ProjectPG
import qualified Module.Project.Service as ProjectService

main :: IO () 
main = do 
  pgEnv <- PG.init 
  let runner app = flip runReaderT pgEnv $ unAppT app 
  HTTP.main runner

type Env = PG.Env

newtype AppT a = AppT 
  { unAppT :: ReaderT Env IO a 
  } deriving (  Applicative, Functor, Monad
              , MonadIO, MonadReader Env)

instance ProjectHTTP.Service AppT where
  getProjectService = ProjectService.getProjectById
  createProjectService = ProjectService.addProjectService
  updateProjectService = ProjectService.updateProjectService
  deleteProjectService = ProjectService.deleteProjectService

instance ProjectService.ProjectRepo AppT where
  findProjectById = ProjectPG.findProjectById
  insertProject = ProjectPG.insertProject
  updateProject = ProjectPG.updateProject
  deleteProjectById = ProjectPG.deleteProjectById

