{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Platform.HTTP where 

import Prelude ()
import ClassyPrelude 
import Web.Scotty.Trans
import Network.HTTP.Types.Status
import Network.Wai (Response)
import Network.Wai.Handler.Warp (defaultSettings, setPort)
import Network.Wai.Middleware.Cors

import qualified Module.Project.HTTP as ProjectHTTP

import System.Environment

type App r m = (ProjectHTTP.Service m, MonadIO m)

main :: (App r m) => (m Response -> IO Response) -> IO ()
main runner = do 
  port <- acquirePort
  scottyT port runner routes
  where
    acquirePort = do
      port <- fromMaybe "" <$> lookupEnv "PORT"
      return . fromMaybe 3000 $ readMay port

-- * Routing 

routes :: (App r m) => ScottyT LText m ()
routes = do 
  -- middlewares 
  middleware $ cors $ const $ Just simpleCorsResourcePolicy
    { corsRequestHeaders = simpleHeaders
    , corsMethods = "PUT":"DELETE":simpleMethods 
    }
  options (regex ".*") $ return ()

  -- err
  defaultHandler $ \str -> do 
    status status500
    json str

  -- module routes
  ProjectHTTP.routes
  -- health check
  get "health" $ json True
