{-# LANGUAGE OverloadedStrings #-}

module Common.PGBuilder where 

import Data.List
import qualified Data.ByteString.UTF8 as BU
import qualified Data.ByteString as B
import Database.PostgreSQL.Simple.Types

formatColumnText :: String -> String 
formatColumnText col = "\"" ++ col ++ "\""

formatQueryColumns :: [String] -> String
formatQueryColumns cols = "(" ++ sCols ++ ")"
  where 
    sCols = intercalate ", " $ map formatColumnText cols
  
formatUpdateColumns :: [String] -> String 
formatUpdateColumns = intercalate ", " . map (\col -> "\"" ++ col ++ "\" = ?") 

buildInsertQuery :: String -> [String] -> Query 
buildInsertQuery tableName columns = 
  Query $ BU.fromString $ "insert into " ++ tableName ++ formatQueryColumns columns ++ "values (" ++ sParams ++ ") returning *"
  where
    qs = replicate (length columns) "?"
    sParams = intercalate ", " qs
  
buildFindByIdQuery :: String -> [String] -> Query 
buildFindByIdQuery tableName cols = 
  Query $ BU.fromString $ "select " ++ sCols ++ " from " ++ tableName ++ " where id = ? limit 1"
  where 
    sCols = intercalate ", " $ map formatColumnText cols
