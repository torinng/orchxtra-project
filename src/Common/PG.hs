{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Common.PG where 

import Prelude ()
import ClassyPrelude hiding (length)

import Data.Has
import Data.Pool
import Data.List 
import Data.Maybe
import Database.PostgreSQL.Simple
import System.Environment

type Env = Pool Connection

type PG r m = (MonadReader r m, Has Env r, MonadIO m)
    
init :: IO Env
init = acquirePool

acquirePool :: IO (Pool Connection)
acquirePool = do
  envUrl <- lookupEnv "DATABASE_URL"
  let pgUrl = fromString $ fromMaybe "postgresql://postgres:123456@localhost/orchxtra_dev" envUrl
  createPool (connectPostgreSQL pgUrl) close 1 10 10

withConn :: PG r m => (Connection -> IO a) -> m a
withConn action = do
  pool <- asks getter
  liftIO $ withResource pool action
