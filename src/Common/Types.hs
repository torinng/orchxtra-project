{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Common.Types where

import Common.AesonUtil
import Data.Text
import Data.Text.Encoding
import Data.Map

import Database.PostgreSQL.Simple.FromField
import Database.PostgreSQL.Simple.ToField
import qualified  Data.ByteString.Char8 as BC
import qualified Data.Text.Lazy as L
type UUID = Text
type UserId = Text
type ServiceAccountId = Text

-- Common Model Status
data Status 
  = Active 
  | Disabled 
  | Deleted
  deriving (Eq)

instance FromField Status where 
  fromField _ = return . maybe Deleted (getStatus . decodeUtf8)

instance ToField Status where 
  toField = Escape . BC.pack . show

instance Show Status where 
  show Active = "active"
  show Disabled = "disabled"
  show Deleted = "deleted"

getStatus :: Text -> Status 
getStatus "active" = Active
getStatus "deleted" = Deleted
getStatus _ = Disabled

-- Common Header transfered from Authorizer
headerRequestId :: L.Text 
headerRequestId = "x-orchxtra-apigateway-request-id"

headerCognitoUsername :: L.Text 
headerCognitoUsername = "x-orchxtra-cognito-username"

headerServiceAccountId :: L.Text 
headerServiceAccountId = "x-orchxtra-service-account-id"

-- Pagination State 
data Pagination = Pagination
  { paginationPage :: Int
  , paginationSize :: Int
  } deriving (Eq, Show)

type InputViolations = Map Text [Text]

newtype ErrorsWrapper a = ErrorsWrapper { errorsWrapperErrors :: a } deriving (Eq, Show)

$(commonJSONDeriveMany
  [ ''ErrorsWrapper
  , ''Status
  ])
