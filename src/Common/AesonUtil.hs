{-# LANGUAGE OverloadedStrings #-}

module Common.AesonUtil where

import Prelude ()
import ClassyPrelude
import Data.Maybe
import Data.Aeson.TH
import Data.Ratio (denominator, numerator)

import Language.Haskell.TH.Syntax
import Data.Scientific (Scientific)

import qualified Text.Digestive.Form as DF
import qualified Text.Digestive.Types as DF
import Text.Digestive.Form ((.:))

commonJSONDeriveMany :: [Name] -> Q [Dec]
commonJSONDeriveMany names =
  concat <$> mapM commonJSONDerive names

commonJSONDerive :: Name -> Q [Dec]
commonJSONDerive name =
  let lowerCaseFirst (y:ys) = toLower [y] ++ ys 
      lowerCaseFirst "" = ""
      structName = fromMaybe "" . lastMay . splitElem '.' . show $ name
  in deriveJSON defaultOptions{fieldLabelModifier = lowerCaseFirst . drop (length structName)} name
  

validateInteger :: Num a => Scientific -> DF.Result Text a
validateInteger x =
  let xRat = toRational x
  in if denominator xRat /= 1
        then DF.Error "Number must be an integer"
        else return (fromInteger $ numerator xRat)

parseInteger :: (Monad m, Num a) => DF.Form Text m a
parseInteger =
  DF.validate validateInteger (DF.stringRead "Could not parse number" Nothing)

parseIntegerOptional :: (Monad m, Num a) => DF.Form Text m (Maybe a)
parseIntegerOptional =
  DF.validateOptional validateInteger (DF.stringRead "Could not parse number" Nothing)

