{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

module Module.Project.Types where 

import Common.Types
import Common.AesonUtil
import Data.Text
import Data.Time.Clock
import Data.Time.Calendar
import Database.PostgreSQL.Simple.Types
import Database.PostgreSQL.Simple.FromRow

type ProjectId = Text  

projectTableName :: String
projectTableName = "project.projects"

data ProjectFilter = ProjectFilter
  { projectFilterKeyword :: Text
  , projectFilterStatus :: Status 
  } deriving (Eq, Show)

data Project = Project 
  { projectId :: ProjectId
  , projectServiceAccountId :: ServiceAccountId
  , projectName :: Text
  , projectAbstract :: Text
  , projectPhase :: Text
  , projectYear :: Int

  , projectScopeOfWork :: Text
  , projectObjective :: Text
  , projectStartDate :: Day 
  , projectEndDate :: Day
  , projectCreatedAt :: UTCTime 

  , projectUpdatedAt :: UTCTime
  , projectStatus :: Status 

  } deriving (Eq, Show)

data CreateProject = CreateProject
  { createProjectName :: Text
  , createProjectAbstract :: Text 
  , createProjectPhase :: Text
  , createProjectYear :: Int
  , createProjectScopeOfWork :: Text

  , createProjectObjective :: Text 
  , createProjectStartDate :: Day
  , createProjectEndDate :: Day
  , createProjectStatus :: Status 
  } deriving (Eq, Show)

data UpdateProject = UpdateProject 
  { updateProjectName :: Maybe Text
  , updateProjectAbstract :: Maybe Text 
  , updateProjectPhase :: Maybe Text
  , updateProjectYear :: Maybe Int
  , updateProjectScopeOfWork :: Maybe Text

  , updateProjectObjective :: Maybe Text 
  , updateProjectStartDate :: Maybe Day
  , updateProjectEndDate :: Maybe Day
  , updateProjectStatus :: Maybe Status 
  } deriving (Eq, Show)

newtype ProjectWrapper a = ProjectWrapper 
  { projectWrapperProject :: a 
  } deriving (Eq, Show)

data ProjectsWrapper a = ProjectsWrapper 
  { projectsWrapperProjects :: [a]
  , projectsWrapperProjectsCount :: Int 
  } deriving (Eq, Show)

data ProjectError 
  = ProjectErrorNotFound UUID
  | ProjectErrorNotAllow UUID
  | ProjectPGError

$(commonJSONDeriveMany
  [ ''ProjectFilter
  , ''Project
  , ''CreateProject
  , ''UpdateProject
  , ''ProjectError
  , ''ProjectWrapper
  , ''ProjectsWrapper
  ])

instance FromRow Project where
  fromRow = Project
    <$> field 
    <*> field
    <*> field
    <*> field
    <*> field
    <*> field
    
    <*> field
    <*> field
    <*> field
    <*> field
    <*> field
    
    <*> field
    <*> field
