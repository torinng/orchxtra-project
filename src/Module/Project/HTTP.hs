{-# LANGUAGE OverloadedStrings #-}
module Module.Project.HTTP where 

import Prelude ()
import ClassyPrelude hiding (delete)
import Module.Project.Types 
import Common.HTTP 
import Common.AesonUtil 
import Common.Types as C
import Web.Scotty.Trans 
import Network.HTTP.Types.Status 
import qualified Text.Digestive.Form as DF 
import qualified Text.Digestive.Types as DF 
import Text.Digestive.Form ((.:))
import Text.Digestive.Types (Result(..))

class Monad m => Service m where 
  getProjectService :: ProjectId -> m (Either ProjectError Project) 
  createProjectService :: C.ServiceAccountId -> CreateProject -> m (Either ProjectError Project) 
  updateProjectService :: C.ServiceAccountId -> ProjectId -> UpdateProject -> m (Either ProjectError Project) 
  deleteProjectService :: C.ServiceAccountId -> ProjectId -> m (Either ProjectError ()) 

routes :: (Service m, MonadIO m) => ScottyT LText m ()
routes = do

  post "/projects" $ do 
    serviceAccountId <- header C.headerServiceAccountId
    body <- parseJsonBody ("project" .: createProjectForm)
    case serviceAccountId of 
      Nothing -> projectErrorHandler (ProjectErrorNotAllow "serviceAccountId")
      Just saId -> do
        result <- stopIfError projectErrorHandler $ createProjectService (toStrict saId) body
        json $ ProjectWrapper result

  get "/projects/:id" $ do 
    id <- param "id"
    result <- lift $ getProjectService id 
    case result of 
      Right project -> json $ ProjectWrapper project
      Left (ProjectErrorNotFound _) -> status status204

  put "/projects/:id" $ do 
    id <- param "id"
    serviceAccountId <- header C.headerServiceAccountId
    case serviceAccountId of 
      Nothing -> projectErrorHandler (ProjectErrorNotAllow id)
      Just saId -> do

        body <- parseJsonBody ("project" .: updateProjectForm)
        result <- lift $ updateProjectService (toStrict saId) id body
        case result of 
          Right project -> json $ ProjectWrapper project
          Left (ProjectErrorNotFound _) -> status status204
  
  delete "/projects/:id" $ do 
    id <- param "id"
    serviceAccountId <- header C.headerServiceAccountId
    case serviceAccountId of 
      Nothing -> projectErrorHandler (ProjectErrorNotAllow id)
      Just saId -> do 
        stopIfError projectErrorHandler $ deleteProjectService (toStrict saId) id 
        status status204
        


-- * Errors 
projectErrorHandler :: (ScottyError e, Monad m) => ProjectError -> ActionT e m ()
projectErrorHandler err = case err of 
  ProjectErrorNotFound  _ -> do 
    status status400
    json err 
  ProjectErrorNotAllow _ -> do 
    status status403
    json err
  ProjectPGError -> do 
    status status500
    json err


-- * Forms
statusValidation :: Text -> DF.Result Text C.Status
statusValidation = return . C.getStatus 

createProjectForm :: (Monad m) => DF.Form Text m CreateProject
createProjectForm = 
  CreateProject <$> "name" .: DF.text Nothing
                <*> "abstract" .: DF.text Nothing
                <*> "phase" .: DF.text Nothing
                <*> "year" .: parseInteger
                <*> "scopeOfWork" .: DF.text Nothing
                <*> "objective" .: DF.text Nothing
                <*> "startDate" .: DF.dateFormlet "%Y-%m-%d" Nothing
                <*> "endDate" .: DF.dateFormlet "%Y-%m-%d" Nothing
                <*> "status" .: DF.validate statusValidation (DF.text Nothing)
                                  
updateProjectForm :: (Monad m) => DF.Form Text m UpdateProject
updateProjectForm = 
  UpdateProject <$> "name" .: DF.optionalText Nothing
                <*> "abstract" .: DF.optionalText Nothing
                <*> "phase" .: DF.optionalText Nothing
                <*> "year" .: parseIntegerOptional
                <*> "scopeOfWork" .: DF.optionalText Nothing
                <*> "objective" .: DF.optionalText Nothing
                <*> "startDate" .: DF.optionalDateFormlet "%Y-%m-%d" Nothing
                <*> "endDate" .: DF.optionalDateFormlet "%Y-%m-%d" Nothing
                <*> "status" .: DF.validateOptional statusValidation (DF.optionalText Nothing)

