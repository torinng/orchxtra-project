module Module.Project.Service where

import Control.Monad.Except
import Module.Project.Types 
import Common.Types 

class (Monad m) => ProjectRepo m where 
  insertProject :: ServiceAccountId -> CreateProject -> m [Project]
  updateProject :: ServiceAccountId -> ProjectId -> UpdateProject -> m [Project]
  findProjectById :: ProjectId -> m [Project] 
  deleteProjectById :: ServiceAccountId -> ProjectId -> m () 


getProjectById :: (ProjectRepo m) => ProjectId -> m (Either ProjectError Project)
getProjectById pId = runExceptT $ do 
  result <- lift $ findProjectById pId
  case result of 
    [project] -> return project 
    _ -> throwError $ ProjectErrorNotFound pId 

addProjectService :: (ProjectRepo m) => ServiceAccountId -> CreateProject -> m (Either ProjectError Project)
addProjectService serviceId form = runExceptT $ do
  result <- lift $ insertProject serviceId form 
  case result of 
    [project] -> return project
    _ -> throwError ProjectPGError

updateProjectService :: (ProjectRepo m) => ServiceAccountId -> ProjectId -> UpdateProject -> m (Either ProjectError Project)
updateProjectService sId pId form = runExceptT $ do 
  result <- lift $ updateProject sId pId form 
  case result of 
      [project] -> return project
      _ -> throwError ProjectPGError


deleteProjectService :: (ProjectRepo m) => ServiceAccountId -> ProjectId -> m (Either ProjectError ())
deleteProjectService sId pId = do 
  deleteProjectById sId pId 
  return $ Right ()
      