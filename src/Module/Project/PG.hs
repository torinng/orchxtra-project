{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Module.Project.PG where
  
import Module.Project.Types
import Common.PG
import Common.PGBuilder
import Common.Types
import Control.Monad
import Database.PostgreSQL.Simple.SqlQQ
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.Types
import qualified Data.ByteString.UTF8 as BU

insertProject :: PG r m => ServiceAccountId -> CreateProject -> m [Project]
insertProject serviceId params = 
  withConn $ \conn -> query conn qry
    ( serviceId
    , createProjectName params
    , createProjectAbstract params
    , createProjectPhase params
    , createProjectYear params 

    , createProjectScopeOfWork params 
    , createProjectObjective params 
    , createProjectStartDate params 
    , createProjectEndDate params 
    , createProjectStatus params
    )

  where 
    qry = buildInsertQuery projectTableName
      [ "serviceAccountId"
      , "name"
      , "abstract"
      , "phase"
      , "year"

      , "scopeOfWork"
      , "objective"
      , "startDate"
      , "endDate"
      , "status"
      ]

findProjectById :: PG r m => ProjectId -> m [Project]
findProjectById pId = 
  withConn $ \conn -> query conn qry (Only pId)
  where
    qry = buildFindByIdQuery projectTableName 
      [ "id"
      , "serviceAccountId"
      , "name"
      , "abstract"
      , "phase"

      , "year"
      , "scopeOfWork"
      , "objective"
      , "startDate"
      , "endDate"

      , "createdAt"
      , "updatedAt"
      , "status"
      ]

updateProject :: PG r m => ServiceAccountId -> ProjectId -> UpdateProject -> m [Project]
updateProject sId pId form = 
    withConn $ \conn -> query conn qry $
      ( updateProjectName form
      , updateProjectAbstract form
      , updateProjectPhase form
      , updateProjectYear form 
      , updateProjectScopeOfWork form 
      , updateProjectObjective form 
      , updateProjectStartDate form
      , updateProjectEndDate form 
      , updateProjectStatus form 
      ) :. (pId, sId)
  where 
    qry = Query . BU.fromString $ "update " ++ projectTableName 
      ++ " set \
          \ name = coalesce(?, name), \
          \ abstract = coalesce(?, abstract), \
          \ phase = coalesce(?, phase), \
          \ year = coalesce(?, year), \
          \ \"scopeOfWork\" = coalesce(?, \"scopeOfWork\"), \
          \ objective = coalesce(?, objective), \
          \ \"startDate\" = coalesce(?, \"startDate\"), \
          \ \"endDate\" = coalesce(?, \"endDate\"), \
          \ status = coalesce(?, status), \
          \ updatedAt = now() \
          \ where id = ? and \"serviceAccountId\" = ?"

deleteProjectById :: PG r m => ServiceAccountId -> ProjectId -> m ()
deleteProjectById sId pId =
  void . withConn $ \conn -> execute conn qry ( show Deleted, pId, sId )
    where
      qry = Query . BU.fromString $ "update " ++ projectTableName 
        ++ " set status = ?, updatedAt = now() \
          \ where id = ? and \"serviceAccountId\" = ?"
